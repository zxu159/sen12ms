import torch
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.utils.data import DataLoader
from collections import defaultdict

import copy
import time
from fastprogress import master_bar, progress_bar
import matplotlib.pyplot as plt
import numpy as np

def print_metrics(metrics, epoch_samples, phase):    
    outputs = []
    for k in metrics.keys():
        outputs.append("{}: {:4f}".format(k, metrics[k] / epoch_samples))
        
    print("{}: {}".format(phase, ", ".join(outputs)))    

def train_model(model, dataloaders, optimizer, scheduler, calc_loss, prefix="unet", num_epochs=25, num_samples=None):
    
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    best_model_wts = copy.deepcopy(model.state_dict())
    best_loss = 1e10
    
    mb = master_bar(range(num_epochs))

    loss_curve = []
    x = []
    
    loss_curve_val = []
    x_val = []
    
    if num_samples is None:
        num_samples = len(dataloaders['train'].dataset)
    
    for epoch in mb:
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)
        
        since = time.time()

        # Each epoch has a training and validation phase
        try:
            for phase in ['train', 'val']:
                if phase == 'train':
                    scheduler.step()
                    for param_group in optimizer.param_groups:
                        print("LR", param_group['lr'])

                    model.train()  # Set model to training mode
                else:
                    model.eval()   # Set model to evaluate mode

                metrics = defaultdict(float)
                epoch_samples = 0
             
                for inputs, labels in progress_bar(dataloaders[phase], parent=mb):
                    inputs = inputs.to(device)
                    labels = labels.to(device)             

                    # zero the parameter gradients
                    optimizer.zero_grad()

                    # forward
                    # track history if only in train
                    with torch.set_grad_enabled(phase == 'train'):
                        outputs = model(inputs)
                        loss = calc_loss(outputs, labels, metrics)

                        # backward + optimize only if in training phase
                        if phase == 'train':
                            loss.backward()
                            optimizer.step()
                            
                    # statistics
                    epoch_samples += inputs.size(0)
                    
                    if phase == 'train':
                        sample_loss = metrics['loss']/epoch_samples
                        x.append(epoch*num_samples + epoch_samples)
                        loss_curve.append(sample_loss)
                    
                    if epoch_samples % 10:
                        x_bounds = [0, epoch_samples]
                        y_bounds = [min(loss_curve),0.85]

                        mb.update_graph([[x, loss_curve ], [x_val, loss_curve_val ]], x_bounds, y_bounds)
                    mb.child.comment = "Loss: {}".format(metrics['loss'] / epoch_samples)
                    #pbar.set_description("Loss %f" % (metrics['loss'] / epoch_samples) )

                print_metrics(metrics, epoch_samples, phase)
                epoch_loss = metrics['loss'] / epoch_samples
                
                # deep copy the model
                if phase == 'val':
                    
                    if epoch_loss < best_loss:
                        best_loss = epoch_loss
                        best_model_wts = copy.deepcopy(model.state_dict())
                    
                    torch.save(model, "{}_model_{}.pt".format(prefix, epoch))
                    
                    x_val.append(epoch*num_samples)
                    loss_curve_val.append(epoch_loss)
                
                    np.save("{}_loss_curve.txt".format(prefix), np.array([[x, loss_curve ], [x_val, loss_curve_val ]]))

        except KeyboardInterrupt:
            break

        time_elapsed = time.time() - since
        print('{:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best val loss: {:4f}'.format(best_loss))
    
    model.load_state_dict(best_model_wts)
    torch.save(model, "unet_model_final.pt".format(epoch))
    np.save("unet_loss_curve.txt", np.array([[x, loss_curve ], [x_val, loss_curve_val ]]))

    # load best model weights
    
    return model, (x, loss_curve)